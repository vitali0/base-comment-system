import os

DEBUG = False

APP_NAME = 'base-comment-system-test'

DB_USER_NAME = 'bcs'
DB_USER_PASSWORD = ''
DB_HOST = '127.0.0.1'
DB_PORT = '5432'
DB_NAME = 'bcs_v1_test'
DB_MIN_POOL_SIZE = 5
DB_MAX_POOL_SIZE = 20
DB_URL = 'postgres://{}:{}@{}:{}/{}'.format(DB_USER_NAME, DB_USER_PASSWORD, DB_HOST, DB_PORT, DB_NAME)

APP_PATH = os.path.abspath(os.path.dirname(__file__))

OPTIONS_HEADERS = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, DELETE',
    'Access-Control-Allow-Headers': 'Authorization, Content-Type',
}

try:
    from bcs.settings_test_local import *

except:
    pass

from datetime import datetime

import sqlalchemy as sa
from sqlalchemy.sql import func as sa_func
from sanic import Blueprint
from webargs_sanic.sanicparser import use_args

from bcs.lib.rest.resource import AuthResource
from bcs.lib.rest.helpers import get_blueprint_name
from bcs.lib.rest.marshal import marshal_collection_with, marshal_with
from bcs.blueprints.comments.models import Comment, CommentHistory, HistoryActionEnum
from bcs.blueprints.comments.schema import CommentReqSchema, FullTreeCommentReqSchema, CommentRespSchema,\
    CreateCommentReqSchema, EditCommentReqSchema, DeleteCommentReqSchema, UserCommentReqSchema, \
    CommentHistoryReqSchema, CommentHistoryRespSchema

__all__ = (
    'bp',
)

bp_name = get_blueprint_name(__name__)
bp = Blueprint(bp_name, url_prefix='/{}'.format(bp_name))


class CommentResourceBase(AuthResource):
    def get_comments_query(self, comment_id=None, entity=None, entity_id=None, last_id=None,
                           page_size=None, user_id=None, full_tree=False):
        where_list = [Comment.deleted_at.is_(None)]

        if not full_tree:
            if comment_id is not None:
                where_list.append(Comment.id == comment_id)

            else:
                where_list += [
                    Comment.entity == entity,
                    Comment.entity_id == entity_id,
                ]

        else:
            if comment_id is not None:
                comment_ids = [comment_id]

            else:
                comment_ids = sa.select([sa_func.array_agg(Comment.id)]) \
                    .select_from(Comment.__table__).where(sa.and_(
                        Comment.entity == entity,
                        Comment.entity_id == entity_id,
                        Comment.deleted_at.is_(None),
                    ))

            where_list.append(Comment.path.overlap(comment_ids))

        if last_id is not None:
            where_list.append(Comment.id > last_id)

        if user_id is not None:
            where_list.append(Comment.user_id == user_id)

        query = Comment.__table__.select().where(sa.and_(*where_list)) \
            .order_by(Comment.path)

        if page_size is not None:
            query = query.limit(page_size)

        return query


class CommentListResource(CommentResourceBase):
    @use_args(CommentReqSchema)
    @marshal_collection_with(CommentRespSchema)
    async def _get(self, request, params, *args, **kwargs):
        async with request.app.db.acquire() as conn:
            query = self.get_comments_query(**params)
            rows = await conn.fetch(query)

            return rows

    async def get_parent_comment_path(self, conn, comment_id):
        if comment_id is None:
            return []

        parent_comment_query = self.get_comments_query(comment_id=comment_id)
        parent_comment_record = await conn.fetchrow(parent_comment_query)
        if not parent_comment_record:
            return self.raise_api_data_error(error_message='Bad comment_id')

        return parent_comment_record['path']

    async def get_new_comment_id(self, conn):
        new_comment_id_query = sa.select('*').select_from(sa_func.nextval('comments_id_seq'))
        new_comment_id_record = await conn.fetchrow(new_comment_id_query)
        return new_comment_id_record['nextval']

    @use_args(CreateCommentReqSchema)
    @marshal_with(CommentRespSchema)
    async def _post(self, request, params, *args, **kwargs):
        async with request.app.db.transaction() as conn:
            parent_comment_path = await self.get_parent_comment_path(conn, params.get('comment_id'))
            new_comment_id = await self.get_new_comment_id(conn)

            new_comment_dict = params.copy()
            if 'comment_id' in new_comment_dict:
                del new_comment_dict['comment_id']

            new_comment_dict.update({
                'id': new_comment_id,
                'user_id': self.user_id,
                'path': parent_comment_path + [new_comment_id]
            })

            create_query = Comment.__table__.insert().values(new_comment_dict).returning(sa.literal_column('*'))
            new_comment_record = await conn.fetchrow(create_query)

            return new_comment_record

    def get_update_comment_query(self, comment_id, **values):
        return Comment.__table__.update().where(Comment.id == comment_id) \
            .values(values).returning(sa.literal_column('*'))

    async def edit_user_comment(self, conn, edit_action, comment_id, check_children=False, **update_values):
        comment_query = self.get_comments_query(comment_id=comment_id, user_id=self.user_id)
        comment_record = await conn.fetchrow(comment_query)
        if not comment_record:
            return self.raise_api_data_error(error_message='Bad comment_id or user_id')

        if check_children:
            comments_query = self.get_comments_query(comment_id=comment_id, full_tree=True, page_size=2)
            rows = await conn.fetch(comments_query)
            if len(rows) > 1:
                return self.raise_api_data_error(error_message='Comment has children')

        comment_changes_diff = {}
        for k, v in update_values.copy().items():
            if comment_record[k] == v:
                del update_values[k]

            else:
                comment_changes_diff[k] = (comment_record[k], v)

        if not update_values:
            return comment_record

        update_query = self.get_update_comment_query(comment_id=comment_id, **update_values)
        updated_comment_record = await conn.fetchrow(update_query)

        comment_history_dict = {
            'user_id': self.user_id,
            'comment_id': comment_id,
            'action': edit_action,
            'diff': CommentHistory.prepare_diff_value(comment_changes_diff),
        }
        new_comment_history_query = CommentHistory.__table__.insert().values(comment_history_dict)
        await conn.fetchrow(new_comment_history_query)

        return updated_comment_record

    @use_args(EditCommentReqSchema)
    @marshal_with(CommentRespSchema)
    async def _put(self, request, params, *args, **kwargs):
        async with request.app.db.transaction() as conn:
            return await self.edit_user_comment(conn, edit_action=HistoryActionEnum.EDIT, **params)

    @use_args(DeleteCommentReqSchema)
    @marshal_with(CommentRespSchema)
    async def _delete(self, request, params, *args, **kwargs):
        async with request.app.db.transaction() as conn:
            return await self.edit_user_comment(conn, edit_action=HistoryActionEnum.DELETE,
                                                deleted_at=datetime.now(), check_children=True, **params)


class FullTreeCommentListResource(CommentResourceBase):
    @use_args(FullTreeCommentReqSchema)
    @marshal_collection_with(CommentRespSchema)
    async def _get(self, request, params, *args, **kwargs):
        async with request.app.db.acquire() as conn:
            query = self.get_comments_query(full_tree=True, **params)
            rows = await conn.fetch(query)

            return rows


class UserCommentListResource(CommentResourceBase):
    def get_user_get_comments_query(self, user_id):
        return Comment.__table__.select() \
            .where(sa.and_(Comment.deleted_at.is_(None), Comment.user_id == user_id)) \
            .order_by(Comment.path)

    @use_args(UserCommentReqSchema)
    @marshal_collection_with(CommentRespSchema)
    async def _get(self, request, params, *args, **kwargs):
        async with request.app.db.acquire() as conn:
            query = self.get_user_get_comments_query(**params)
            rows = await conn.fetch(query)

            return rows


class CommentHistoryListResource(AuthResource):
    def get_comment_history_query(self, comment_id):
        return CommentHistory.__table__.select() \
            .where(CommentHistory.comment_id == comment_id) \
            .order_by(CommentHistory.id)

    @use_args(CommentHistoryReqSchema)
    @marshal_collection_with(CommentHistoryRespSchema)
    async def _get(self, request, params, *args, **kwargs):
        async with request.app.db.acquire() as conn:
            query = self.get_comment_history_query(**params)
            rows = await conn.fetch(query)

            return rows


bp.add_route(CommentListResource.as_view(), '/')
bp.add_route(FullTreeCommentListResource.as_view(), '/full')
bp.add_route(UserCommentListResource.as_view(), '/user')
bp.add_route(CommentHistoryListResource.as_view(), '/history')

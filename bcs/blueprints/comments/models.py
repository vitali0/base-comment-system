import enum
from datetime import date, datetime

from sqlalchemy import Column, String, BigInteger, Enum, CheckConstraint, Index, text as sa_text, ForeignKey
from sqlalchemy.dialects.postgresql import ARRAY, JSONB

from bcs.lib.db.models import Base
from bcs.lib.db.mixins import TimestampedModelMixin, CreatedAtModelMixin

__all__ = (
    'Comment',
    'CommentHistory',
    'EntityEnum',
    'HistoryActionEnum',
)


class EntityEnum(enum.Enum):
    USER_PAGE = 'USER_PAGE'
    ARTICLE = 'ARTICLE'


class HistoryActionEnum(enum.Enum):
    EDIT = 'EDIT'
    DELETE = 'DELETE'


class Comment(TimestampedModelMixin, Base):
    __tablename__ = 'comments'

    id = Column(BigInteger, autoincrement=True, primary_key=True)
    user_id = Column(BigInteger, nullable=False)

    entity = Column(Enum(EntityEnum, name='entities'), nullable=True)
    entity_id = Column(BigInteger, nullable=True)

    text = Column(String, nullable=False)
    path = Column(ARRAY(BigInteger), nullable=False, default=[], server_default=sa_text('array[]::bigint[]'))

    __table_args__ = (
        CheckConstraint('(entity is not null and entity_id is not null) or (entity is null and entity_id is null)',
                        name='chk_entity_c'),
        Index('comment_path_idx', 'path', postgresql_using='gin',
              postgresql_where=sa_text('comments.deleted_at is null')),
        Index('comment_user_id_idx', 'user_id', postgresql_where=sa_text('comments.deleted_at is null')),
    )


class CommentHistory(CreatedAtModelMixin, Base):
    __tablename__ = 'comments_history'

    id = Column(BigInteger, autoincrement=True, primary_key=True)
    user_id = Column(BigInteger, nullable=False)

    comment_id = Column(BigInteger, ForeignKey('comments.id'), nullable=False)
    action = Column(Enum(HistoryActionEnum, name='history_actions'), nullable=False)
    diff = Column(JSONB, nullable=False, default={}, server_default=sa_text("'{}'::jsonb"))  # {key: (old, new)}

    __table_args__ = (
        Index('comments_history_comment_id_idx', 'comment_id'),
    )

    @staticmethod
    def prepare_diff_value(raw_diff):
        result = {}

        for k, (old_v, new_v) in raw_diff.items():
            if isinstance(old_v, (datetime, date)):
                old_v = old_v.isoformat()

            if isinstance(new_v, (datetime, date)):
                new_v = new_v.isoformat()

            result[k] = (old_v, new_v)

        return result

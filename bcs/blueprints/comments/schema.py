from bcs.lib.rest.schema import Schema, fields, validates_schema, ValidationError
from bcs.blueprints.comments.models import EntityEnum

__all__ = (
    'CommentReqSchema',
    'FullTreeCommentReqSchema',
    'CommentRespSchema',
    'CreateCommentReqSchema',
    'EditCommentReqSchema',
    'DeleteCommentReqSchema',
    'UserCommentReqSchema',
    'CommentHistoryReqSchema',
    'CommentHistoryRespSchema',
)


class CommentReqSchema(Schema):
    entity = fields.EnumField(EntityEnum, required=True)
    entity_id = fields.Int(required=True)
    last_id = fields.Int(missing=0)
    page_size = fields.Int(missing=10)


class FullTreeCommentReqSchema(Schema):
    comment_id = fields.Int()
    entity = fields.EnumField(EntityEnum)
    entity_id = fields.Int()

    @validates_schema
    def validate_req_params(self, data):
        req_params_msg = 'Input data must have a "entity" and "entity_id" keys or only "comment_id" key'

        if ('entity' in data or 'entity_id' in data) and 'comment_id' in data:
            raise ValidationError(req_params_msg)

        elif not ({'entity', 'entity_id', 'comment_id'} & set(data)):
            raise ValidationError(req_params_msg)

        elif len({'entity', 'entity_id'} & set(data)) == 1:
            raise ValidationError('Input data must have a "entity" and "entity_id" keys')

        return data


class CreateCommentReqSchema(FullTreeCommentReqSchema):
    text = fields.Str(required=True)


class UserCommentReqSchema(Schema):
    user_id = fields.Int(required=True)


class DeleteCommentReqSchema(Schema):
    comment_id = fields.Int(required=True)


class EditCommentReqSchema(DeleteCommentReqSchema):
    text = fields.Str(required=True)


class CommentHistoryReqSchema(Schema):
    comment_id = fields.Int(required=True)


class CommentRespSchema(Schema):
    id = fields.Int()
    user_id = fields.Int()

    entity = fields.Str()
    entity_id = fields.Int()

    text = fields.Str()
    path = fields.List(fields.Int())

    created_at = fields.DateTime()
    updated_at = fields.DateTime()


class CommentHistoryRespSchema(Schema):
    id = fields.Int()
    user_id = fields.Int()

    comment_id = fields.Int()
    action = fields.Str()
    diff = fields.Dict(keys=fields.Str(), values=fields.Raw())

    created_at = fields.DateTime()

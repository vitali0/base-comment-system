from bcs import settings
from bcs.application import create_app

if __name__ == '__main__':
    app = create_app(settings)
    app.run(
        host=app.config.SERVER_HOST,
        port=app.config.SERVER_PORT,
        workers=app.config.WORKERS_COUNT,
        debug=app.config.DEBUG,
    )

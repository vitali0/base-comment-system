from sanic import Blueprint

from bcs.api.v1 import api_blueprint_v1

__all__ = (
    'api_blueprint',
)

api_blueprint = Blueprint.group(
    api_blueprint_v1,
    # api_blueprint_v2,
    url_prefix='/api'
)

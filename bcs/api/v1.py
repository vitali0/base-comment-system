from sanic import Blueprint

from bcs.blueprints.comments.rest import bp as comments_bp

__all__ = (
    'api_blueprint_v1',
)

api_blueprint_v1 = Blueprint.group(
    comments_bp,
    url_prefix='/v1'
)

from marshmallow import Schema as _Schema, validates_schema, ValidationError


class Schema(_Schema):
    class Meta:
        strict = True

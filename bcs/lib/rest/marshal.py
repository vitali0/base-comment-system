import functools

from asyncpg import Record

__all__ = (
    'marshal_with',
    'marshal_collection_with',
)


def marshal_with(schema, many=False):
    def decorator(func):
        @functools.wraps(func)
        async def wrapped(*args, **kwargs):
            rows = await func(*args, **kwargs)

            if isinstance(rows, Record):
                rows = dict(rows)

            return {
                'success': schema(many=many).dump(rows).data,
            }

        return wrapped

    return decorator


def marshal_collection_with(schema):
    return marshal_with(schema, True)

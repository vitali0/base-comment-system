__all__ = (
    'SuccessResponse',
    'get_blueprint_name',
)


class SuccessResponse:
    success = True

    def __init__(self, success=True):
        self.success = success

    def is_success(self):
        return self.success


def get_blueprint_name(module_name):
    return module_name.rsplit('.', 2)[-2]

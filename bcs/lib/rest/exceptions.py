from bcs.lib.json_utils import json

__all__ = (
    'AppError',
    'AuthError',
    'ApiError',
    'ApiDataError',
    'ServerError',
    'ServerDataError',
)


class AppError(Exception):
    error_text = 'server_error'
    status_code = 200

    def __init__(self, error=None, status_code=None, **response_data):
        self.alert_text = response_data.get('alert_text')
        self.status_code = status_code or self.status_code

        self.response_data = response_data
        self.response_data['error'] = error or self.error_text

        super(AppError, self).__init__(self.response_data['error'])

    def __str__(self):
        return json.dumps(self.response_data, ensure_ascii=False, indent=2)


class AuthError(AppError):
    error_text = 'auth_error'
    status_code = 401


class ApiError(AppError):
    error_text = 'api_error'
    status_code = 400


class ApiDataError(ApiError):
    error_text = 'not_valid_input'
    status_code = 400


class ServerError(AppError):
    error_text = 'server_error'
    status_code = 500


class ServerDataError(ServerError):
    error_text = 'server_data_error'
    status_code = 404

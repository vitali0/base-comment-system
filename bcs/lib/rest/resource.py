import re
import uuid

from sanic import response as sanic_resp
from sanic.log import logger
from sanic.views import HTTPMethodView
from webargs_sanic.sanicparser import HandleValidationError

from bcs.lib.rest.exceptions import AppError, ApiError, AuthError, ApiDataError, ServerError, ServerDataError
from bcs.lib.rest.helpers import SuccessResponse

__all__ = (
    'Resource',
    'AuthResource',
)

user_id_re = re.compile(r'^([1-9]\d*|0)$')


class Resource(HTTPMethodView):
    async def get(self, request, *args, **kwargs):
        return await self._handle_request(request, self._get, *args, **kwargs)

    async def post(self, request, *args, **kwargs):
        return await self._handle_request(request, self._post, *args, **kwargs)

    async def put(self, request, *args, **kwargs):
        return await self._handle_request(request, self._put, *args, **kwargs)

    async def delete(self, request, *args, **kwargs):
        return await self._handle_request(request, self._delete, *args, **kwargs)

    async def options(self, request, *args, **kwargs):
        return sanic_resp.raw(
            '',
            status=204,
            headers=request.app.config.OPTIONS_HEADERS,
        )

    async def _get(self, request, *args, **kwargs):
        return self.raise_api_error(
            error_reason='not valid method',
            method='GET',
            url=self.request.url
        )

    async def _post(self, request, *args, **kwargs):
        return self.raise_api_error(
            error_reason='not valid method',
            method='POST',
            url=self.request.url
        )

    async def _put(self, request, *args, **kwargs):
        return self.raise_api_error(
            error_reason='not valid method',
            method='PUT',
            url=self.request.url
        )

    async def _delete(self, request, *args, **kwargs):
        return self.raise_api_error(
            error_reason='not valid method',
            method='DELETE',
            url=self.request.url
        )

    def raise_api_error(self, **kwargs):
        raise ApiError(**kwargs)

    def raise_api_data_error(self, **kwargs):
        raise ApiDataError(**kwargs)

    def raise_server_error(self, **kwargs):
        raise ServerError(**kwargs)

    def raise_server_data_error(self, **kwargs):
        raise ServerDataError(**kwargs)

    def raise_wrong_input_error(self, param):
        return self.raise_api_data_error(
            errors_info={
                param: 'Something wrong',
            }
        )

    async def _handle_request(self, request, handler, *args, **kwargs):
        self.request_id = uuid.uuid4().hex

        try:
            response_data = await handler(request, *args, **kwargs)

            if isinstance(response_data, SuccessResponse):
                response_data = {
                    'success': response_data.is_success(),
                }

            status_code = 200

        except HandleValidationError as err:
            new_err = ApiDataError(error_message=err.exc.messages)
            response_data = new_err.response_data
            status_code = new_err.status_code

        except (AppError, ApiDataError) as err:
            response_data = err.response_data
            status_code = err.status_code
            logger.error(err, exc_info=True)

        except Exception as err:
            status_code = 500
            response_data = {'error': 'server_error'}
            if request.app.debug:
                response_data['error_message'] = 'Something wrong'

            logger.error(err, exc_info=True)

        return self._response_handler(response_data, status_code)

    def _response_handler(self, response_data, status_code=200):
        if isinstance(response_data, (dict, list, tuple)):
            return sanic_resp.json(
                response_data,
                headers={'Content-type': 'application/json'},
                status=status_code,
            )

        return response_data


class AuthResource(Resource):
    user_id = None

    async def _handle_request(self, request, handler, *args, **kwargs):
        user_id = request.headers.get('USER_ID')
        if user_id is None or not user_id_re.match(user_id):
            err = AuthError()
            response_data = err.response_data
            status_code = err.status_code
            return self._response_handler(response_data, status_code)

        self.user_id = int(user_id)
        return await super(AuthResource, self)._handle_request(request, handler, *args, **kwargs)

    # def raise_auth_error(self, **kwargs):
    #     raise AuthError(**kwargs)

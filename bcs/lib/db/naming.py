__all__ = (
    'naming_convention',
)

MAX_POSTGRESQL_NAME_LEN = 63


def truncated_name(table, suffix):
    table_name = table.name
    suffix = '_{}'.format(suffix)
    if len(table_name) + len(suffix) > MAX_POSTGRESQL_NAME_LEN:
        table_name = table_name[:MAX_POSTGRESQL_NAME_LEN - (len(table_name) + len(suffix))]
    return '{}{}'.format(table_name, suffix)


def ix_ext(constraint, table):
    suffix = '{}_'.format('_'.join(column.name for column in constraint.columns))
    if len(constraint.columns) == 1:
        column = constraint.columns.values()[0]
        if column.foreign_keys:
            suffix = '{}fkey_'.format(suffix)

    if constraint.unique:
        suffix = '{}u'.format(suffix)

    suffix = '{}idx'.format(suffix)
    return truncated_name(table, suffix)


def uq_ext(constraint, table):
    suffix = '{}_unq'.format('_'.join(column.name for column in constraint.columns))
    return truncated_name(table, suffix)


naming_convention = {
    'ix_ext': ix_ext,
    'ix': '%(ix_ext)s',
    'uq_ext': uq_ext,
    'uq': '%(uq_ext)s',
    'fk': '%(table_name)s_%(column_0_name)s_fkey',
    'pk': '%(table_name)s_pkey',
}

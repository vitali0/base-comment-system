from asyncpg import create_pool

__all__ = (
    'BaseConnection',
    'TransactionConnection',
    'ConnectionPool',
)


class BaseConnection:
    def __init__(self, pool, conn=None):
        self._pool = pool
        self._conn = conn

    @property
    def rowcount(self):
        return self._conn.rowcount

    async def add_listener(self, channel, callback):
        await self._conn.add_listener(channel, callback)

    async def remove_listener(self, channel, callback):
        await self._conn.remove_listener(channel, callback)

    async def execute(self, query, *args, timeout=None):
        return await self._conn.execute(query, *args, timeout=timeout)

    async def executemany(self, command, args, timeout=None):
        return await self._conn.executemmay(command, args, timeout=timeout)

    async def fetch(self, query, *args, timeout=None):
        return await self._conn.fetch(query, *args, timeout=timeout)

    async def fetchrow(self, query, *args, timeout=None):
        return await self._conn.fetchrow(query, *args, timeout=timeout)

    async def fetchval(self, query, *args, column=0, timeout=None):
        return await self._conn.fetchval(query, *args, column=column, timeout=timeout)

    async def prepare(self, query, *args, timeout=None):
        return await self._conn.prepare(query, *args, timeout=timeout)

    async def set_builtin_type_codec(self, typename, *args, schema='public', codec_name):
        await self._conn.set_builtin_type_codec(typename, *args, schema=schema, codec_name=codec_name)

    async def set_type_codec(self, typename, *args, schema='public', encoder, decoder, binary=False):
        await self._conn.set_type_codec(typename, *args, schema=schema, encoder=encoder, decoder=decoder, binary=binary)

    def transaction(self, *args, isolation='read_committed', readonly=False, deferrable=False):
        return self._conn.transaction(*args, isolation=isolation, readonly=readonly, deferrable=deferrable)

    async def release(self):
        await self._pool.release(self._conn)

    async def close(self):
        await self._conn.close()

    async def __aenter__(self):
        self._conn = await self._get_conn()
        return self

    async def __aexit__(self, exc_type, exc_value, traceback):
        await self.release()

    async def _get_conn(self):
        return await self._pool.acquire() if not self._conn else self._conn


class TransactionConnection(BaseConnection):
    def __init__(self, pool, conn=None):
        super(TransactionConnection, self).__init__(pool, conn)
        self._tr = None

    async def __aenter__(self):
        self._conn = await self._get_conn()
        self._tr = self.transaction()
        await self._tr.start()
        return self

    async def __aexit__(self, exc_type, exc_value, traceback):
        try:
            if exc_type is None:
                await self._tr.commit()
            else:
                await self._tr.rollback()

        except:
            pass

        finally:
            await self.release()


class ConnectionPool:
    def __init__(self, loop=None):
        self._loop = loop
        self._conn = None
        self._pool = None

    async def init(self, config, max_size, conn=None):
        if conn:
            self._conn = conn

        self._pool = await create_pool(**config, loop=self._loop, max_size=max_size)
        return self

    def acquire(self):
        return BaseConnection(self._pool, self._conn)

    def transaction(self):
        return TransactionConnection(self._pool, self._conn)

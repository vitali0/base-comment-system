from enum import Enum

import sqlalchemy.sql.selectable
import sqlalchemy.sql.dml
from sqlalchemy.dialects import postgresql
from sqlalchemy.sql.sqltypes import DateTime, NullType, String

__all__ = (
    'get_sa_helper',
)


class StringLiteral(String):
    def literal_processor(self, dialect):
        super_processor = super(StringLiteral, self).literal_processor(dialect)

        def process(value):
            if isinstance(value, Enum):
                return repr(value)

            if isinstance(value, int):
                return str(value)

            if not isinstance(value, str):
                value = str(value)
            result = super_processor(value)

            if isinstance(result, bytes):
                result = result.decode(dialect.encoding)

            return result

        return process


class LiteralDialect(postgresql.dialect):
    colspecs = {
        # prevent various encoding explosions
        String: StringLiteral,
        # teach SA about how to literalize a datetime
        DateTime: StringLiteral,
        # don't format py2 long integers to NULL
        NullType: StringLiteral,
    }


def get_sa_helper(cursor):
    class Helper:
        class _ExecuteInPoolMixin:
            async def execute(self):
                sql = self.compile(dialect=LiteralDialect(), compile_kwargs={"literal_binds": True}, inline=True)
                if not isinstance(self, Helper.select):
                    sql = '{} RETURNING *'.format(sql)

                return await cursor.fetch(str(sql))

        class select(_ExecuteInPoolMixin, sqlalchemy.sql.selectable.Select):
            pass

        class insert(_ExecuteInPoolMixin, sqlalchemy.sql.dml.Insert):
            pass

        class delete(_ExecuteInPoolMixin, sqlalchemy.sql.dml.Delete):
            pass

        class update(_ExecuteInPoolMixin, sqlalchemy.sql.dml.Update):
            pass

    return Helper

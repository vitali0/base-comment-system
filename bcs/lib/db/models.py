from sqlalchemy import MetaData
from sqlalchemy.ext.declarative import declarative_base

from bcs.lib.db.naming import naming_convention

__all__ = (
    'Base',
)

Base = declarative_base()
Base.metadata = MetaData(naming_convention=naming_convention)

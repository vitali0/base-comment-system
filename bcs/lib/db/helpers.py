import inspect

__all__ = (
    'parse_models',
)


def parse_models(modules, base_model_class):
    result = {}
    for module_ in modules:
        result.update(
            inspect.getmembers(module_, lambda obj: inspect.isclass(obj) and issubclass(obj, base_model_class))
        )
    return result

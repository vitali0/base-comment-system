from datetime import datetime
from sqlalchemy import text, DateTime, Column

__all__ = (
    'CreatedAtModelMixin',
    'TimestampedModelMixin',
    'BaseMixin',
)


class CreatedAtModelMixin:
    created_at = Column(DateTime, nullable=False, default=datetime.now, server_default=text('now()'))


class TimestampedModelMixin(CreatedAtModelMixin):
    updated_at = Column(DateTime, nullable=False, default=datetime.now, onupdate=datetime.now,
                        server_default=text('now()'))
    deleted_at = Column(DateTime, nullable=True)

    def touch(self):
        self.updated_at = datetime.now()

    def remove(self):
        self.deleted_at = datetime.now()


class BaseMixin:
    @classmethod
    def make_from_record(cls, record):
        res = cls.__call__()

        for c in cls.__table__.columns:
            if c.name in record:
                setattr(res, c.name, record[c.name])

        return res

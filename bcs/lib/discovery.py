import os
from fnmatch import fnmatchcase
from importlib import import_module
from pkgutil import iter_modules

from bcs import settings

__all__ = (
    'discover_modules',
    'import_modules',
)


def discover_modules(name_wildcard, *packages):
    for package in packages:
        package_path = os.path.join(settings.APP_PATH, '..', package.replace('.', '/'))

        for _loader, name, is_package in iter_modules([package_path]):
            module_path = '{}.{}'.format(package, name)
            if is_package:
                for module_ in discover_modules(name_wildcard, module_path):
                    yield module_

            elif fnmatchcase(name, name_wildcard):
                yield module_path


def import_modules(name_wildcard, *packages):
    return [import_module(module_) for module_ in discover_modules(name_wildcard, *packages)]

import asyncpgsa

__all__ = (
    'create_one_transaction_pool',
)


async def create_one_transaction_pool(**params):
    pool = await asyncpgsa.create_pool(**params)

    tr = pool.transaction()
    conn = await tr.__aenter__()

    return Wrapper(conn, tr.transaction)


class Wrapper:
    _conn = None
    _tr = None
    _wr_conn = None

    def __init__(self, conn, tr):
        self._conn = conn
        self._tr = tr
        self._wr_conn = self.WrappedConnection(self._conn)

    def acquire(self):
        return self._wr_conn

    def transaction(self):
        return self._wr_conn

    async def rollback(self):
        self._tr._managed = False
        await self._tr.rollback()

    class WrappedConnection:
        _conn = None

        def __init__(self, conn):
            self._conn = conn

        async def __aenter__(self):
            return self._conn

        async def __aexit__(self, exc_type, exc, tb):
            pass

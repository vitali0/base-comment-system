import sqlalchemy as sa

from bcs.tests.base_test_resource import BaseTestResource
from bcs.blueprints.comments.models import Comment

comment_table = Comment.__table__


class TestCommentListResource(BaseTestResource):
    async def test_create_root_comment(self, test_cli, app):
        headers = {'user_id': '100'}
        params = {
            'entity_id': 100,
            'entity': 'ARTICLE',
            'text': 'good article!',
        }

        resp = await test_cli.post('/api/v1/comments/',
                                   params=params, headers=headers)
        assert resp.status == 200
        resp_json = await resp.json()
        assert resp_json['success']['text'] == 'good article!'
        assert resp_json['success']['entity_id'] == 100
        assert resp_json['success']['entity'] == 'ARTICLE'

        async with app.db.acquire() as conn:
            c_query = comment_table.select().where(Comment.id == resp_json['success']['id'])
            c_record = await conn.fetchrow(c_query)
            assert c_record is not None

            count_query = sa.select([sa.func.count()]).select_from(comment_table)
            count_record = await conn.fetchrow(count_query)
            assert count_record['count_1'] == 1

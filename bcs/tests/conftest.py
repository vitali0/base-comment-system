import pytest

from bcs.application import create_app
from bcs.lib.tests.db_pool import create_one_transaction_pool
from bcs import settings_test


@pytest.yield_fixture
def app():
    app_ = create_app(settings_test, disable_logging=True,
                      create_db_pool=create_one_transaction_pool)
    yield app_

    # app_.db.rollback()


@pytest.fixture
def test_cli(loop, app, test_client):
    return loop.run_until_complete(test_client(app))

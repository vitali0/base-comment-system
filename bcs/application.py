import json

from sanic import Sanic
from sanic.log import LOGGING_CONFIG_DEFAULTS
import asyncpgsa
from asyncpgsa.connection import get_dialect

from bcs import api
from bcs.lib.json_utils import json as fast_json

__all__ = (
    'create_app',
)


def create_app(settings, disable_logging=False, create_db_pool=asyncpgsa.create_pool):
    log_config = LOGGING_CONFIG_DEFAULTS
    if disable_logging:
        log_config['loggers'] = {}

    else:
        log_config['loggers']['asyncpgsa.query'] = {
            'level': 'DEBUG' if settings.DEBUG else 'INFO',
            'handlers': ['console'],
        }

    app = Sanic(settings.APP_NAME, log_config=log_config)
    app.config.from_object(settings)
    app.blueprint(api.api_blueprint)

    @app.listener('before_server_start')
    async def setup_db_connection_pool(app, loop):
        async def set_jsonb_charset(connection):
            await connection.set_type_codec(
                'jsonb',
                encoder=json.dumps,
                decoder=fast_json.loads,
                schema='pg_catalog'
            )

        dialect = get_dialect(
            json_serializer=json.dumps,
            json_deserializer=fast_json.loads
        )

        app.db = await create_db_pool(
            loop=loop,
            host=settings.DB_HOST,
            port=settings.DB_PORT,
            database=settings.DB_NAME,
            user=settings.DB_USER_NAME,
            password=settings.DB_USER_PASSWORD,
            min_size=settings.DB_MIN_POOL_SIZE,
            max_size=settings.DB_MAX_POOL_SIZE,
            dialect=dialect,
            init=set_jsonb_charset,
        )

    return app

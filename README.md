Installation
============

Virtualenv creation.

```
$ mkvirtualenv bcs
$ workon bcs
```

Install requirements.
```
$ pip install -r etc/requirements.txt
```

Configuring.

```
$ vim bcs/settings_local.py
```

Init db.

```
$ ./etc/bin/init_db.sh
```


Running
============

Run server.

```
$ ./etc/bin/run_server.sh
```

Run tests.

```
$ ./etc/bin/run_tests.sh
```


Sample requests
===============

Create comment:

```
$ curl -X POST "http://localhost:8000/api/v1/comments/" \
  -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
  -H 'user_id: 100' \
  -F entity=ARTICLE \
  -F entity_id=146 \
  -F 'text=hello world'

$ curl -X POST "http://localhost:8000/api/v1/comments/" \
  -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
  -H 'user_id: 100' \
  -F text=today \
  -F comment_id=511272
```

Get first level comments:

```
$ curl -X GET 'http://localhost:8000/api/v1/comments/?entity_id=146&entity=ARTICLE' -H 'user_id: 100'
```

Get full tree comments:

```
$ curl -X GET 'http://localhost:8000/api/v1/comments/full?entity_id=146&entity=ARTICLE' -H 'user_id: 100'
```

Get user comments:

```
$ curl -X GET 'http://localhost:8000/api/v1/comments/user?entity_id=146&entity=ARTICLE&user_id=5' -H 'user_id: 100'
```

Edit comment:

```
$ curl -X PUT 'http://localhost:8000/api/v1/comments/?comment_id=511272&text=apple' -H 'user_id: 100'
```

Delete comment:

```
$ curl -X DELETE 'http://localhost:8000/api/v1/comments/?comment_id=511272' -H 'user_id: 100'
```

Get comment history:

```
$ curl -X GET 'http://localhost:8000/api/v1/comments/history?comment_id=511272' -H 'user_id: 100'
```

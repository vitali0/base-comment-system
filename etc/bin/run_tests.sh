#!/usr/bin/env bash

set -e
set -o pipefail
#set -x

script_dir=$(cd $(dirname $0) && pwd)
project_dir="$script_dir/../../bcs"

cd ${project_dir}
PYTHONPATH=.. alembic -x settings=settings_test upgrade head

PYTHONPATH=.. pytest .

